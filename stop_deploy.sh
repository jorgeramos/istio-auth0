#!/bin/bash
echo "*** Stop deploy ***"
if [ $# -eq 0 ]
then
  echo "No image provided"
  exit 1
fi

for var in "$@"
do
  echo "Stoping deployment $var in AWS EC2 instance"
  OUTPUT="$(/snap/bin/microk8s.kubectl delete deployment $var)"
  echo "$OUTPUT"
done

echo "*** Finish ***"