provider "aws" {
 
  region     = "us-west-2"
  access_key = "AKIAIPZBHLYYRLRFEWRA"
  secret_key = "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"
 
}
 
resource "aws_instance" "awsresources" {
  ami         = "ami-06f2f779464715dc5"
  instance_type = "t3a.2xlarge"
  key_name   = "jrp_key"

  private_ip = "10.0.0.12"
  subnet_id  = "${aws_subnet.tf_jrp_subnet.id}"

  tags = {
    Name = "EC2-JRAMOS-TF"
  }

  root_block_device {
    volume_size = "32"
  }
}

resource "aws_vpc" "main" {
  cidr_block           = "10.0.0.0/16"
  enable_dns_hostnames = true
}

resource "aws_internet_gateway" "gw" {
  vpc_id = "${aws_vpc.main.id}"
}

resource "aws_subnet" "tf_jrp_subnet" {
  vpc_id                  = "${aws_vpc.main.id}"
  cidr_block              = "10.0.0.0/24"
  map_public_ip_on_launch = true

  depends_on = ["aws_internet_gateway.gw"]
}

resource "aws_eip" "lb" {
  vpc                       = true
  instance                  = "${aws_instance.awsresources.id}" 
  associate_with_private_ip = "10.0.0.12"
  depends_on = ["aws_internet_gateway.gw"]
}

resource "aws_security_group" "allow_tls" {
  name        = "allow_tls"
  description = "Allow TLS inbound traffic"
  vpc_id      = "${aws_vpc.main.id}"

  ingress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port = 8080
    to_port = 8080
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }
}

resource "aws_elb" "elb-jrp" {
  name               = "elb-jrp"
  availability_zones = ["us-west-2a", "us-west-2b", "us-west-2c"]
  security_groups    = ["${aws_security_group.allow_tls.id}"]

  listener {
    instance_port     = "80"
    instance_protocol = "http"
    lb_port           = "80"
    lb_protocol       = "http"
  }

  listener {
    instance_port     = "8080"
    instance_protocol = "http"
    lb_port           = "8080"
    lb_protocol       = "http"
  }

  health_check {
    healthy_threshold   = 10
    unhealthy_threshold = 2
    timeout             = 5
    target              = "HTTP:80/"
    interval            = 30
  }

  instances                   = ["${aws_instance.awsresources.id}"]
  cross_zone_load_balancing   = true
  idle_timeout                = 400
  connection_draining         = true
  connection_draining_timeout = 400

  tags = {
    Name = "elb-jrp"
  }
}

resource "aws_ecr_repository" "awsresources" {
  name = "awsresources"
}
