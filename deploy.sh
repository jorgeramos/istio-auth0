#!/bin/bash
echo "*** Start deploy ***"
if [ $# -eq 0 ]
then
  echo "No image provided"
  exit 1
fi

for var in "$@"
do
  echo "Deploying $var into AWS EC2 instance"
  /snap/bin/microk8s.kubectl run $var --image=ramper84/$var:latest --port=9080
  echo "Creating $var Gateway"
  /snap/bin/microk8s.kubectl apply -f deployments/$var-gateway.yaml
  OUTPUT="$(/snap/bin/microk8s.kubectl get deployment $var)"
  echo "$OUTPUT"    
done

echo "*** Finish ***" 